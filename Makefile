ifeq ($(PREFIX),)
	PREFIX := /usr/local
endif

FILES = `ls bin/`
AUTOCOMPLETE = `ls autocomplete`

build:
	@echo "No need to build. Just install it!"

install: bin/* autocomplete/*
	@install -d $(DESTDIR)$(PREFIX)/bin/
	@install -m 755 bin/* $(DESTDIR)$(PREFIX)/bin/
	@install -d /etc/bash_completion.d/
	@install -m 644 autocomplete/* /etc/bash_completion.d/

uninstall:
	@for f in $(FILES); do \
		rm $(DESTDIR)$(PREFIX)/bin/$$f ; \
	done
